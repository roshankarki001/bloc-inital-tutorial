import 'dart:convert';

import 'package:flutter_bloc_tutorial/user_model.dart';
import 'package:http/http.dart';

class UserRepository {
  Future<List<UserModel>> getUsers({required String id}) async {
    String userUrl = 'https://reqres.in/api/users?page=$id';
    Response response = await get(Uri.parse(userUrl));

    if (response.statusCode == 200) {
      final List result = jsonDecode(response.body)['data'];
      return result.map((e) => UserModel.fromJson(e)).toList();
    } else {
      throw Exception(response.reasonPhrase);
    }
  }
}
