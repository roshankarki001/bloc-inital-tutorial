import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_bloc_tutorial/user_bloc.dart';
import 'package:flutter_bloc_tutorial/user_model.dart';
import 'package:flutter_bloc_tutorial/user_states.dart';

class NextScreen extends StatelessWidget {
  const NextScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: BlocBuilder<UserBloc, UserState>(builder: (context, state) {
      if (state is UserLoadingState) {
        return const Center(
          child: CircularProgressIndicator(),
        );
      } else if (state is UserErrorState) {
        return Center(
          child: Text(state.error),
        );
      } else if (state is UserLoadedState) {
        List<UserModel> userList = state.users;
        return ListView.builder(
            itemCount: userList.length,
            itemBuilder: (_, index) {
              return Padding(
                padding: const EdgeInsets.symmetric(vertical: 4, horizontal: 8),
                child: Card(
                    color: Theme.of(context).primaryColor,
                    child: ListTile(
                        title: Text(
                          '${userList[index].firstName}  ${userList[index].lastName}',
                          style: const TextStyle(color: Colors.white),
                        ),
                        subtitle: Text(
                          '${userList[index].email}',
                          style: const TextStyle(color: Colors.white),
                        ),
                        leading: CircleAvatar(
                          backgroundImage:
                              NetworkImage(userList[index].avatar.toString()),
                        ))),
              );
            });
      }
      return const SizedBox();
    }));
  }
}
