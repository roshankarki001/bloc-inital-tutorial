import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_bloc_tutorial/api_repo.dart';
import 'package:flutter_bloc_tutorial/next_screen.dart';
import 'package:flutter_bloc_tutorial/user_bloc.dart';
import 'package:flutter_bloc_tutorial/user_events.dart';
import 'package:flutter_bloc_tutorial/user_model.dart';
import 'package:flutter_bloc_tutorial/user_states.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<UserBloc>(create: (_) => UserBloc(UserRepository()))
      ],
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          // This is the theme of your application.
          //
          // Try running your application with "flutter run". You'll see the
          // application has a blue toolbar. Then, without quitting the app, try
          // changing the primarySwatch below to Colors.green and then invoke
          // "hot reload" (press "r" in the console where you ran "flutter run",
          // or simply save your changes to "hot reload" in a Flutter IDE).
          // Notice that the counter didn't reset back to zero; the application
          // is not restarted.
          primarySwatch: Colors.blue,
        ),
        home: const MyHomePage(title: 'Flutter Demo Home Page'),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  void initState() {
    super.initState();
    BlocProvider.of<UserBloc>(context).add(LoadUserEvent('1'));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Column(
        children: [
          TextField(
            onChanged: (value) {
              context.read<UserBloc>().add(LoadUserEvent(value));
            },
          ),
          BlocBuilder<UserBloc, UserState>(builder: (context, state) {
            if (state is UserLoadingState) {
              return const Center(
                child: CircularProgressIndicator(),
              );
            } else if (state is UserErrorState) {
              return Center(
                child: Text(state.error),
              );
            } else if (state is UserLoadedState) {
              List<UserModel> userList = state.users;
              return Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  ListView.builder(
                      shrinkWrap: true,
                      itemCount: userList.length,
                      itemBuilder: (_, index) {
                        return Padding(
                          padding: const EdgeInsets.symmetric(
                              vertical: 4, horizontal: 8),
                          child: Card(
                              color: Theme.of(context).primaryColor,
                              child: ListTile(
                                  title: Text(
                                    '${userList[index].firstName}  ${userList[index].lastName}',
                                    style: const TextStyle(color: Colors.white),
                                  ),
                                  subtitle: Text(
                                    '${userList[index].email}',
                                    style: const TextStyle(color: Colors.white),
                                  ),
                                  leading: CircleAvatar(
                                    backgroundImage: NetworkImage(
                                        userList[index].avatar.toString()),
                                  ))),
                        );
                      }),
                ],
              );
            }
            return const SizedBox();
          }),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (_) => NextScreen()));
        },
      ),
    );
  }
}
