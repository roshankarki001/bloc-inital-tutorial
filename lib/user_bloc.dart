import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_bloc_tutorial/api_repo.dart';
import 'package:flutter_bloc_tutorial/user_events.dart';
import 'package:flutter_bloc_tutorial/user_states.dart';

class UserBloc extends Bloc<UserEvent, UserState> {
  final UserRepository _userRepository;
  UserBloc(this._userRepository) : super(UserLoadingState()) {
    on<LoadUserEvent>((event, emit) async {
      emit(UserLoadingState());
      try {
        final users = await _userRepository.getUsers(id: event.id);
        emit(UserLoadedState(users));
      } catch (ex) {
        emit(UserErrorState(ex.toString()));
      }
    });
  }
}
