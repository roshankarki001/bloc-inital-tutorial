import 'package:equatable/equatable.dart';

abstract class UserEvent extends Equatable {
  const UserEvent();
}

class LoadUserEvent extends UserEvent {
  final String id;
  const LoadUserEvent(this.id);
  @override
  List<Object?> get props => [id];
}
